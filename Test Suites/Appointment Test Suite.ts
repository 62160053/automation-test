<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Appointment Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>05540580-09bb-451b-bbc3-b39bbe99b403</testSuiteGuid>
   <testCaseLink>
      <guid>a1c9220c-b3dc-48fe-8e5e-79d6f52f3717</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f580f169-42b2-4337-bd80-62a6a220ca16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginFail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b6282aa-2256-48c2-a454-6a8c9f72453b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Change</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c1aa71b-6ef7-4fd1-9ec5-99ebaa5523f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login 8Charater</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7a077a3-fa64-415d-ba38-e09322e5e8d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password not Charater</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e7f22f5-0ab8-4375-8cb2-25a93dbd3863</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password Not Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c803fae-791b-4cd9-a4dd-6cd77d88e3b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password Not Special Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc6f27ac-b687-49df-b211-4b2a1ea52f06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
